package tools;

import java.util.ArrayList;

public class Combiner {

	/**
	 * OK
	 * 
	 * @param <T>
	 * @param motList
	 * @param longeur
	 * @return
	 */
	public static <T> ArrayList<ArrayList<T>> combinaisons(ArrayList<T> motList, int longeur) {
		ArrayList<ArrayList<T>> resultat = new ArrayList<>();
        for (int i = 0; i < motList.size(); i++) {
            if (longeur == 1) {
            	ArrayList<T> list = new ArrayList<>();
            	list.add(motList.get(i));
                resultat.add(list);
            }
            else {
            	
                ArrayList<T> listIntermediaire = new ArrayList<>(motList.subList(i + 1, motList.size()));
                ArrayList<ArrayList<T>> combiList = combinaisons(listIntermediaire, longeur - 1);
                for (ArrayList<T> s : combiList) {
                	s.add(motList.get(i));
                	resultat.add(s);
                }
            }
        }
        return resultat;
    }
	
	/**
	 * OK
	 * 
	 * @param motList
	 * @return
	 */
	public static <T> ArrayList<ArrayList<T>> combinaisonsAll(ArrayList<T> motList){
		ArrayList<ArrayList<T>> resultat = new ArrayList<>();
		for(int i = 1; i<motList.size()+1; i++) {
			resultat.addAll(combinaisons(motList, i));
		}
		return resultat;
	}
}
