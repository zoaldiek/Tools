package tools;

import java.io.Serializable;

/**
 * This class is a tuple, with two element.
 * @author Negroni Orso
 *
 * @param <L> any object.
 * @param <R> any object.
 */
public class Couple<L, R> implements Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3840757296331321512L;
	private L left;
	private R right;

	/**
	 * The constructor of the {@link Couple}. 
	 * 
	 * @param left, the first element.
	 * @param right, the second element.
	 */
	public Couple(L left, R right) {
		this.left = left;
		this.right = right;
	}

	/**
	 * Get the first element.
	 * 
	 * @return L
	 */
	public L getLeft() {
		return left;
	}

	/**
	 * Get the second element.
	 * 
	 * @return R
	 */
	public R getRight() {
		return right;
	}
	
	public void setLeft(L left) {
		this.left = left;
	}
	
	public void setRight(R right) {
		this.right = right;
	}

	@Override
	public int hashCode() {
		return left.hashCode() ^ right.hashCode();
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Couple))
			return false;
		Couple<L, R> pairo = (Couple<L, R>) o;
		return this.left.equals(pairo.getLeft()) && this.right.equals(pairo.getRight());
	}
	


}