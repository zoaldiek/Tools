package tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains all methods who work with the files.
 * 
 * @author Negroni Orso
 *
 */
public class FileTools {
	/**
	 * * test if a file exist with this file name * * @param fileName , the file
	 * name * * @return b , true if the file exist, false else
	 */
	public static boolean fileExists(String fileName) {
		File ff = new File(fileName);
		return (ff.exists() && !ff.isDirectory());
	}

	/**
	 * * create a file * * @param fileName , the file name * * @return b , true if
	 * the creating of the file is a sucess
	 */
	public static boolean createFile(String fileName) {
		boolean b = false;
		File ff = new File(fileName);
		try {
			if (ff.createNewFile())
				b = true;
			else {
				System.out.println("\nThe file " + fileName + " already exists!\n");
				b = true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return b;
	}
	
	public static boolean deleteFile(String fileName) {
		boolean b = false;
		File ff = new File(fileName);
		if(ff.exists()) {
			b = ff.delete();
		}
		
		return b;
	}
	
	public static boolean deleteFolder(String folderName) {
		boolean b = false;
		File ff = new File(folderName);
		if(ff.exists()) {
			if(ff.isDirectory()) {
				File[] children = ff.listFiles();
				for(int i = 0; i<children.length;i++) {
					deleteFolder(children[i].getPath());
				}
			}
			b = ff.delete();
		}
		return b;
	}

	/**
	 * * write a text in a file * * @param fileName , the file name * @param buf ,
	 * the text to write in the file
	 */
	public static boolean writeFile(String fileName, String buf) {
		try {
			File file = new File(fileName);
			FileWriter fileW = new FileWriter(file);
			fileW.write(buf);
			fileW.close();
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	/**
	 * * read the content of the file line by line * * @param fileName , the file
	 * name * * @return lines , the lines of the file on form of List<String>, each
	 * String are a line
	 */
	public static List<String> loadFileByLine(String fileName) {
		List<String> lines = new ArrayList<String>();
		if (!FileTools.fileExists(fileName)) {
			try {
				throw new IOException("The file " + fileName + " doesn't exist!");
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		} else {
			try {
				FileReader f = new FileReader(fileName);
				BufferedReader br = new BufferedReader(f);
				String line;
				while ((line = br.readLine()) != null) {
					lines.add(line);
				}
				f.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		return lines;
	}
	
	/**
	 * * read the content of the file line by line * * @param fileName , the file
	 * name * * @return lines , the lines of the file on form of List<String>, each
	 * String are a line
	 */
	public static String loadFileByBuffer(String fileName) {
		String buff = "";
		if (!FileTools.fileExists(fileName)) {
			try {
				throw new IOException("The file " + fileName + " doesn't exist!");
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		} else {
			try {
				FileReader f = new FileReader(fileName);
				BufferedReader br = new BufferedReader(f);
				String line;
				while ((line = br.readLine()) != null) {
					buff += line+"\n";
				}
				f.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		return buff;
	}
}
